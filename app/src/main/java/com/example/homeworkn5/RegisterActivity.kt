package com.example.homeworkn5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPassword1: EditText
    private lateinit var buttonSignup: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListener()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword1 = findViewById(R.id.editTextPassword1)
        buttonSignup = findViewById(R.id.buttonSignup)

    }

    private fun registerListener() {
        buttonSignup.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val password1 = editTextPassword1.text.toString()

            if (email.isEmpty() || password.isEmpty() || password1.isEmpty()) {
                Toast.makeText(this, "შეავსეთ ყველა ველი!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!email.contains("@")) {
                Toast.makeText(this, "გთხოვთ სწორად შეიყვანოთ E-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password != password1) {
                Toast.makeText(this, "პაროლები არ ემთხვევა!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9) {
                Toast.makeText(this, "პაროლი უნდა შედგებოდეს 9 ან მეტი სიმბოლოსგან!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.contains(Regex("[0-9]")) && password.contains(Regex("[a-z]|[A-Z]"))) {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password,)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            startActivity(Intent(this, LoginActivity::class.java))
                            finish()
                        }else
                            Toast.makeText(this, "დაფიქსირდა შეცდომა", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

